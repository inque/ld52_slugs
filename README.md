
## About

https://ldjam.com/events/ludum-dare/54/slugs

## Building

### Visual Studio Code

* Install "CMake Tools" extension.
* Open this directory
* Press F7 to build, Shift+F5 to build and run, or F5 to debug.
* Ctrl+Shift+P -> "CMake: Select Variant" to choose between debug and release profile.

### CMake

```
mkdir build
cd build
cmake ..
make
```

### Web (WASM)

* Requires [Emscripten](https://emscripten.org/), CMake and Ninja
* The `data` folder should not be empty. Remove `--preload-file ../data@data` in cmake source if you're not planning to use it.
* Run `misc\build_web.bat`
* This should produce `build_web\src\mygame.wasm.js` and `.wasm`
* Put it in on your web server together with `misc\index.html`
