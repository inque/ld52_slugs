#include "gameapp.h"
#include "unity_build.h"
#include "raylib.h"
#include "string.h"
#include "stdio.h"
#ifdef __EMSCRIPTEN__
	#include <emscripten.h>
#elif _MSC_VER
	#ifdef NDEBUG
		//turn off console in Release mode in MSVC
		#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
	#endif
#endif

static void ParseArgs(int argc, char** argv){
	if(argc <= 1) return;
	for(int i = 1; i < argc; i++){
		char* arg = argv[i];
		arg += (*arg == '-');
		arg += (*arg == '-');
		switch(arg[0]){
			case 's':
				app.headless = true;
				break;
			case 'c':
				i++;
				app.configPath = argv[i];
				break;
			case 'h':
				printf(
					"Command line options: \n"
					"  --server\n"
					"  --c [config path]\n\n"
				);
				break;
		}
	}
}

static void InitializeApp(){
	LoadSettings();

	if(app.headless){
		//dedicated server
		LoadAssets();
		printf("Server is running on port potato.\n");
	}else{
	  	//regular client
		CreateGameWindow();
		ApplyPlatformSpecificHacks();
		LoadAssets();
	}
}

static void UpdateGame(){
	static unsigned int timeLast;
	unsigned int frameBudget;
	unsigned int timeNow;
	unsigned int timeDiff;
	
	if(!app.headless){
		DrawGraphics();
	}

	frameBudget = (1000 / GET_GAME_TICKRATE());
	timeNow = Millisecs();
	timeDiff = timeNow - timeLast;
	while(timeDiff >= frameBudget){
		GameTick();
		timeDiff -= frameBudget;
		timeLast = timeNow;
		if(timeDiff > 200){
			break;
		}
	}
}

static void MainLoop(){
	#ifdef __EMSCRIPTEN__
		emscripten_set_main_loop(UpdateGame, 0, 0);
	#else
		if(app.headless){
			while(true){
				UpdateGame();
				Delay(1);
			}
		}else{
			while(!client.shouldClose){
				UpdateGame();
				if(!client.vsync){
					Delay(1);
				}
				if(WindowShouldClose()) break;
			}
			SaveSettings();
		}
	#endif
}

int main(int argc, char** argv){
	ParseArgs(argc, argv);
	InitializeApp();
	MainLoop();
	return 0;
}
