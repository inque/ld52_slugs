// this header is unfinished

#pragma once
#include "utils.h"
#include "string.h"
#include "stdint.h"
#include "stdbool.h"

enum {
	CNT_OPT_BOUNDCHECKS = 1
};

#define GET_ENTITY(WHOMST, id) (WHOMST##_TYPE*)cnt_CollectionGet(&(WHOMST##_ARRAY), (WHOMST##_ARRAY).nextBlock, (WHOMST##_ARRAY).data, sizeof(WHOMST##_TYPE), id, (WHOMST##_OPTIONS) & CNT_OPT_BOUNDCHECKS, WHOMST##_CAPACITY)
#define GET_ENTITY_FAST(WHOMST, id) (WHOMST##_TYPE*)cnt_CollectionGet(&(WHOMST##_ARRAY), (WHOMST##_ARRAY).nextBlock, (WHOMST##_ARRAY).data, sizeof(WHOMST##_TYPE), id, false, 0)
#define CREATE_ENTITY(WHOMST, idPtr) (WHOMST##_TYPE*)cnt_CollectionInsert(idPtr, &(WHOMST##_ARRAY), (WHOMST##_ARRAY).nextBlock, (WHOMST##_ARRAY).data, (WHOMST##_ARRAY).isActive, sizeof(WHOMST##_TYPE), WHOMST##_CAPACITY)
#define REMOVE_ENTITY(WHOMST, id) cnt_CollectionRemove(&(WHOMST##_ARRAY), (WHOMST##_ARRAY).isActive, id);
#define FOR_EACH(WHOMST, Handle, Id) \
	for(int Id = 0; Id < WHOMST##_CAPACITY; Id++) \
		if(cnt_CollectionIsActive((WHOMST##_ARRAY).isActive, Id)) \
			for(WHOMST##_TYPE *Handle = GET_ENTITY_FAST(WHOMST, Id), *bogus = NULL; bogus == NULL; bogus++)

#ifndef CNT_ACTIVE_FLAG_TYPE
	#if INTPTR_MAX <= INT32_MAX
		// 32-bit word size
		#define CNT_ACTIVE_FLAG_TYPE uint32_t
		#define CNT_ACTIVE_FLAG_POT 5
	#else
		// 64-bit or greater
		#define CNT_ACTIVE_FLAG_TYPE uint64_t
		#define CNT_ACTIVE_FLAG_POT 6
	#endif
#endif

#ifndef CNT_NEXT_BLOCK_CACHE
	/// How many blocks you can jump over at once
	#define CNT_NEXT_BLOCK_CACHE 4
#endif

#define COLLECTION_TYPE(WHOMST) \
    struct { \
        CNT_ACTIVE_FLAG_TYPE isActive[((WHOMST##_CAPACITY - 1) >> CNT_ACTIVE_FLAG_POT) + 1]; \
        WHOMST##_TYPE data[WHOMST##_CAPACITY]; \
        void* nextBlock[(WHOMST##_EXPANSION == 0) ? 1 : CNT_NEXT_BLOCK_CACHE]; \
    }

static inline bool cnt_CollectionIsActive(CNT_ACTIVE_FLAG_TYPE* isActive, int id) {
	int step = id >> CNT_ACTIVE_FLAG_POT;
	int base = step << CNT_ACTIVE_FLAG_POT;
	CNT_ACTIVE_FLAG_TYPE mask = 1 << (id - base);
	return isActive[step] & mask;
}

static inline void cnt_CollectionSetActive(CNT_ACTIVE_FLAG_TYPE* isActive, int id, bool value) {
	int step = id >> CNT_ACTIVE_FLAG_POT;
	int base = step << CNT_ACTIVE_FLAG_POT;
	CNT_ACTIVE_FLAG_TYPE mask = 1 << (id - base);
	CNT_ACTIVE_FLAG_TYPE* out = &isActive[step];

	if(value) *out |= mask;
	else      *out &= ~mask;
}

// static inline void* cnt_CollectionAddBlock(void* collection, int newBlockId) {
// }

// static inline void cnt_CollectionGetBlock(bool create, void* collection, void* nextBlocks, void** array, CNT_ACTIVE_FLAG_TYPE** isActive, int id, int cap, int expansion) {
// }

static inline void* cnt_CollectionGet(void* collection, void* nextBlocks, void* array, size_t elementSize, int id, bool boundCheck, int cap) {
	if(boundCheck && (id >= cap || id < 0))
		return NULL;
	else {
		void* ptr = array;
		AddPtr(&ptr, id * elementSize);
		return ptr;
	}
}

static inline void* cnt_CollectionInsert(int* outId, void* collection, void* nextBlocks, void* array, CNT_ACTIVE_FLAG_TYPE* isActive, size_t elementSize, int cap) {
	void* ptr = array;
	for(int id = 0; id < cap; id++) {
		if(!cnt_CollectionIsActive(isActive, id)) {
			cnt_CollectionSetActive(isActive, id, true);
			AddPtr(&ptr, id * elementSize);
			memset(ptr, 0, elementSize);
			*outId = id;
			return ptr;
		}
	}
	return NULL;
}

static inline void cnt_CollectionRemove(void* collection, CNT_ACTIVE_FLAG_TYPE* isActive, int id) {
	cnt_CollectionSetActive(isActive, id, false);
}
