#include "gameapp.h"
#include "gameplay.h"
#include "macros.h"
#include "gameinterface.h"
#include "raymath.h"
#include "stdbool.h"
#include "time.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#ifdef __EMSCRIPTEN__
	#include <emscripten.h>
#endif


AppState_t app = {
	.configPath = "options.txt",
	.speed = 1
};
GameState_t game;


void LoadAssets(){
}

void GameTick(){

	if(app.shouldRestartGame){
		ResetGame();
		app.shouldRestartGame = false;
	}

	if(!app.headless){
		client.lastTickTime = GetTime();
		if(client.paused) return;
		if(client.screen != SCREEN_GAME) return;
	}

	UpdatePlayers();
	UpdateBoard(&game.board);
	UpdateGameplay();

	game.frame++;
}

void DrawGraphics(){
	if(app.headless) return;
	UpdateClient();
	ClearBackground(GRAY);
	BeginDrawing();
	switch(client.screen){
		case SCREEN_MAINMENU: {
			// MainMenu();
			StartGame();
			client.screen = SCREEN_GAME;
		} break;
		case SCREEN_GAME: {
			LocalPlayerControls();
			DrawBackground(&game.board);
			BeginMode2D(client.cam);
				DrawBoardTiles(&game.board);
				DrawBoardEntities(&game.board);
				DrawBoardGuys(&game.board);
				DrawOverlay();
			EndMode2D();
			BeginMode2D(client.uiCam);
				DrawHUD();
			EndMode2D();
		} break;
	}
	EndDrawing();
}

void StartGame(){
	memset(&game, 0, sizeof(game));
	if(!app.headless){
		client.playerId = CreatePlayer(false, "You");
	}
	ResetGame();
}

void ResetGame(){
	game.state = STATE_PREPARING;
	game.frame = 0;
	game.timer = 0;
	InitializeBoard(&game.board);

	if(app.gamesPlayed++ > 0){
		CloseGame();
		#ifdef __EMSCRIPTEN__
			//web: save settings on every reset
			SaveSettings();
		#endif
	}

	if(!app.headless){
		ResetClient();
	}
}

void CloseGame(){
	forEachPlayer(player, playerId){
		if(player->isBot){
			RemovePlayer(playerId);
		}else{
			ResetPlayerStats(playerId);
		}
	}
}
