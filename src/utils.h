#ifndef UTILS_H
#include "stdbool.h"
#include "stdint.h"

uint64_t rand64(uint64_t* seed);
int rand32(uint64_t* seed, int min, int maxx);
int mod2(int a, int b); /// modulo operator, but negative numbers wrap
int Millisecs();        /// current time in milliseconds
void Delay(int ms);     /// sleep
float SmoothOver(float cv, float st, float dt); /// fps-independent lerp
void ApplyPlatformSpecificHacks();

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#define randf() ((float)rand() / RAND_MAX) /// random float
#define CHR4(d,c,b,a) ( ((a)<<24) | ((b)<<16) | ((c)<<8) | (d) ) /// int32 string

//location approximation for fast search
#define SECTOR_SIZE 512
#define SECTOR_SET(out, vec) out[0] = (int)(vec.x / SECTOR_SIZE); \
                             out[1] = (int)(vec.y / SECTOR_SIZE)
#define IS_SECTOR_FAR(a, b, max) ((abs(a[0]-b[0]) + abs(a[1]-b[1])) > max)
#define IS_SECTOR_OFFSCREEN(a) IS_SECTOR_FAR(a, client.sector, client.sectorsInView)

/// increment pointer by 1 byte
static inline void IncPtr(void** ptr){
	char **tmpPtr = (char**)ptr;
		++*tmpPtr;
}

/// increment pointer by N bytes
static inline void AddPtr(void** ptr, int bytes){
	char **tmpPtr = (char**)ptr;
		  *tmpPtr += bytes;
}

#ifndef _countof
#define _countof(a) (sizeof(a)/sizeof(*(a)))
#endif

#define UTILS_H
#endif
#if defined(IMP_UTILS_H) || defined(__IDE__)
//==== implementation starts here ====

#include "gameapp.h"
#include "stdlib.h"
#include "math.h"
#include "time.h"
#include "stddef.h"
#ifdef _WIN32
    #include "safe_windows.h"
#else
	#include "sys/stat.h"
	#include "utime.h"
	#include "unistd.h"
#endif
#ifdef __EMSCRIPTEN__
	#include <emscripten.h>
	#include <emscripten/html5.h>
#endif

uint64_t rand64(uint64_t* seed){
	// from https://github.com/svaarala/duktape/blob/master/misc/splitmix64.c
	uint64_t z = (*seed += UINT64_C(0x9E3779B97F4A7C15));
	z = (z ^ (z >> 30)) * UINT64_C(0xBF58476D1CE4E5B9);
	z = (z ^ (z >> 27)) * UINT64_C(0x94D049BB133111EB);
	return z ^ (z >> 31);
}

int rand32(uint64_t* seed, int min, int maxx){
	int raw = (int)rand64(seed);
	int alt = mod2(raw, maxx - min);
	return min + alt;
}

int mod2(int a, int b){
	int r = a % b;
	return r < 0 ? r + b : r;
}

int Millisecs(){
	#ifdef _WIN32
		return (int)clock();
	#else
		struct timespec ts;
		clock_gettime(CLOCK_MONOTONIC, &ts);
		static unsigned long long int baseTime;
		unsigned long long int time = (unsigned long long int)ts.tv_sec*1000000000LLU + (unsigned long long int)ts.tv_nsec;
		if(!baseTime){
			baseTime = time;
			return 0;
		}else{
			return (int)((time - baseTime)*1e-9 * 1000);
		}
	#endif
}

void Delay(int ms){
	#ifdef _WIN32
		Sleep(ms);
	#elif defined(__EMSCRIPTEN__)
		return;
	#elif defined(__linux__)
		struct timespec req = { 0 };
		time_t sec = (int)(ms/1000.0f);
		ms -= (float)(sec*1000);
		req.tv_sec = sec;
		req.tv_nsec = ms*1000000L;

		while (nanosleep(&req, &req) == -1) continue;
	#elif defined(__APPLE__)
		usleep(ms*1000.0f);
	#endif
}

float SmoothOver(float cv, float st, float dt){
	return 1 - powf((1 - cv), (dt / st));
}

#ifdef __EMSCRIPTEN__

static EM_BOOL onEmscSizeChanged(int event_type, const EmscriptenUiEvent* ui_event, void* user_data){
	double w, h;
	float dpi_scale;
	emscripten_get_element_css_size("canvas", &w, &h);
	if(w < 1){
		w = ui_event->windowInnerWidth;
	}
	if(h < 1){
		h = ui_event->windowInnerHeight;
	}
	// dpi_scale = emscripten_get_device_pixel_ratio();
	dpi_scale = 1;
	int width = (int)(w*dpi_scale);
	int height = (int)(h*dpi_scale);
	emscripten_set_canvas_element_size("canvas", width, height);

	EM_ASM_INT({
		GLFW.onCanvasResize($0, $1);
	}, width, height);
	return true;
}

#endif

void ApplyPlatformSpecificHacks(){
    #ifdef __EMSCRIPTEN__
        //fix canvas resizing
        emscripten_set_resize_callback(EMSCRIPTEN_EVENT_TARGET_WINDOW, 0, false, onEmscSizeChanged);
    #endif
}


#undef IMP_UTILS_H
#endif
