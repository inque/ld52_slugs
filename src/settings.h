#ifndef SETTINGS_H

// To register a new option, add it in:
// - AppConfig_t
// - DefaultSettings()
// - LoadSettings()
// - SaveSettings()

typedef struct{
	int fullscreen;
	int vsync;
	int resolution[2];
	int windowPos[2];
	float scale;
	float soundVolume;
	float musicVolume;
	int tickrate;
	int interpolation;
	int gamepad;
} AppConfig_t;

void DefaultSettings();
void LoadSettings();
void SaveSettings();


#define SETTINGS_H
#endif
#if defined(IMP_SETTINGS_H) || defined(__IDE__)
//==== implementation starts here ====

#include "gameapp.h"
#include "string.h"
#include "stdlib.h"
#include "stdio.h"
#ifdef __EMSCRIPTEN__
	#include <emscripten.h>
#endif

void DefaultSettings(){
	app.config = (AppConfig_t){
		.fullscreen = 0,
		.vsync = 0,
		.resolution = {1280, 800},
		.windowPos = {-1, -1},
		.scale = 1,
		.soundVolume = 0.725f,
		.musicVolume = 0.675f,
		.tickrate = TICKS_PER_SEC,
		.interpolation = 1,
		.gamepad = 0
	};
}

static void* FindCfgKey(char* cfg, size_t len, const char* key){
	size_t keyLen = strlen(key);
	size_t p = 0;
	while(p < len){
		if(!strncmp(key, cfg, keyLen)){
			while(p < len){
				p++; cfg++;
				if(cfg[-1] == '=') break;
			}
			size_t valLen = 0;
			while((p + valLen) < len){
				valLen++;
				if(cfg[valLen] == 0x0A) break;
				if(cfg[valLen] == 0)    break;
			}
			cfg[valLen] = 0;
			return (void*)cfg;
		}
		while(p < len){
			p++; cfg++;
			if(cfg[-1] == 0x0A) break;
			if(cfg[-1] == 0)   break;
		}
	}
	return NULL;
}

static void GetCfgOption_i(char* cfg, int len, const char* key, int* out){
	void* val = FindCfgKey(cfg, len, key);
	if(val) *out = atoi(val);
}

static void GetCfgOption_f(char* cfg, int len, const char* key, float* out){
	void* val = FindCfgKey(cfg, len, key);
	if(val) *out = atof(val);
}

static void WriteCfgOption_i(const char* key, int value, FILE* file){
	char line[1024];
	int len = sprintf(line, "%s=%i\n", key, value);
	fwrite(line, 1, len, file);
}

static void WriteCfgOption_f(const char* key, float value, FILE* file){
	char line[1024];
	int len = sprintf(line, "%s=%f\n", key, value);
	fwrite(line, 1, len, file);
}

static void SyncBeforeRead();
static void SyncAfterWrite();

#ifdef __EMSCRIPTEN__
	//make the next function callable from js
	EMSCRIPTEN_KEEPALIVE
#endif

void LoadSettings(){
	DefaultSettings();
	SyncBeforeRead();
	
	FILE* file = fopen(app.configPath, "r");
	// if(!file){
	// 	//try user home diretory if not found
	// 	char homePath[256];
	// 	if(HomeFilePath(app.configPath, homePath, sizeof(homePath))){
	// 		file = fopen(homePath, "r");
	// 	}
	// }
	if(file){
		int scale = 100;
		int volume = 70;
		char cfg[8192];
		size_t len = fread(cfg, 1, sizeof(cfg)-1, file);
		cfg[len] = 0;
		fclose(file);

		GetCfgOption_i(cfg, len, "fullscreen",  &app.config.fullscreen);
		GetCfgOption_i(cfg, len, "vsync",       &app.config.vsync);
		GetCfgOption_i(cfg, len, "width",       &app.config.resolution[0]);
		GetCfgOption_i(cfg, len, "height",      &app.config.resolution[1]);
		GetCfgOption_i(cfg, len, "windowpos_x", &app.config.windowPos[0]);
		GetCfgOption_i(cfg, len, "windowpos_y", &app.config.windowPos[1]);
		GetCfgOption_i(cfg, len, "scale", &scale);
		app.config.scale = scale/100.0f;
		GetCfgOption_i(cfg, len, "sound_volume", &volume);
		app.config.soundVolume = volume/100.0f;
		GetCfgOption_i(cfg, len, "music_volume", &volume);
		app.config.musicVolume = volume/100.0f;
		GetCfgOption_i(cfg, len, "gamespeed",     &app.config.tickrate);
		GetCfgOption_i(cfg, len, "interpolation", &app.config.interpolation);
		GetCfgOption_i(cfg, len, "gamepad_num",   &app.config.gamepad);
	}
}

void SaveSettings(){
	FILE* file = fopen(app.configPath, "w");
	if(!app.headless) UpdateWindowSettings();
	// if(!file){
	// 	char homePath[256];
	// 	if(HomeFilePath(app.configPath, homePath, sizeof(homePath))){
	// 		file = fopen(homePath, "w");
	// 	}
	// }
	if(file){
		WriteCfgOption_i("fullscreen",  app.config.fullscreen,    file);
		WriteCfgOption_i("vsync",       app.config.vsync,         file);
		WriteCfgOption_i("width",       app.config.resolution[0], file);
		WriteCfgOption_i("height",      app.config.resolution[1], file);
		WriteCfgOption_i("windowpos_x", app.config.windowPos[0],  file);
		WriteCfgOption_i("windowpos_y", app.config.windowPos[1],  file);
		WriteCfgOption_i("scale",        (int)(app.config.scale*100), file);
		WriteCfgOption_i("sound_volume", (int)(app.config.soundVolume*100), file);
		WriteCfgOption_i("music_volume", (int)(app.config.musicVolume*100), file);
		WriteCfgOption_i("gamespeed",     app.config.tickrate, file);
		WriteCfgOption_i("interpolation", app.config.interpolation, file);
		WriteCfgOption_i("gamepad_num",   app.config.gamepad, file);
		fclose(file);
	}

	SyncAfterWrite();
}

#ifdef __EMSCRIPTEN__
	static void SyncBeforeRead(){
		app.configPath = "/ld58_slugs/config.txt";
		EM_ASM(
			if(Module.syncdone != 1){
				FS.mkdir('/ld58_slugs');
				FS.mount(IDBFS, {}, '/ld58_slugs');
				Module.syncdone = 0;
				FS.syncfs(true, function(err){
					if(err) return console.error(err);
					Module.syncdone = 1;
					Module.ccall('LoadSettings', null, null, null);
				});
			}
		);
	}

	static void SyncAfterWrite(){
		EM_ASM(
			FS.syncfs(false, function(err){if(err) console.error(err);} );
		);
	}
#else
	static void SyncBeforeRead(){};
	static void SyncAfterWrite(){};
#endif

#undef IMP_SETTINGS_H
#endif
