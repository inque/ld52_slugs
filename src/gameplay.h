#ifndef GAMEPLAY_H
#define GAMEPLAY_H

void UpdateGameplay();

#endif
#if defined(IMP_GAMEPLAY_H) || defined(__IDE__)
#undef IMP_GAMEPLAY_H
#include "gameapp.h"

static void SpawnPlayer(Player_t* player, int playerId){
	SpawnPoint_t* point = &game.board.spawnPoints[playerId];
	SpawnGuy(&game.board, playerId, point->x, point->y, 5, point->dx, point->dy);
}

void UpdateGameplay(){

	switch(game.state){

		case STATE_PREPARING:
			// add some bots
			for(int i = 0; i < 10; i++)
				CreatePlayer(true, NULL);

			game.state = STATE_STARTING;
			game.timer = 2 * TICKS_PER_SEC;
			break;
		
		case STATE_STARTING:
			forEachPlayer(player, playerId){
				if(!player->guyCount)
					SpawnPlayer(player, playerId);
			}
			if(!--game.timer){
				game.state = STATE_PLAYING;
				game.timer = 0;
			}
			break;
		
		case STATE_PLAYING: {
			int activePlayers = 0;
			forEachPlayer(player, playerId){
				if(player->guyCount){
					activePlayers++;
				}
			}
			if(activePlayers < 2){
				// end round
				forEachPlayer(player, playerId){
					if(player->guyCount)
						UpdatePlayerRank(playerId);
				}
				game.timer = 20 * TICKS_PER_SEC;
				game.state = STATE_ENDED;
				break;
			}
			game.timer++;
		} break;

		case STATE_ENDED:
			if(!--game.timer){
				ResetGame();
			}
			break;
	}

}

#endif
