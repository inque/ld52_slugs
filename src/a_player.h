#ifndef A_PLAYER_H
#include "raylib.h"
#include "stdbool.h"

#define PLAYERS_TYPE Player_t
#define PLAYERS_CAPACITY 128
#define PLAYERS_EXPANSION 0
#define PLAYERS_OPTIONS 0
#define PLAYERS_ARRAY game.players

typedef struct{
	bool isBot;
	char name[32];
	/// x1, y1, x2, y2
	int aabb[4];
	int guyCount;
	int simulationScore;
	Color color;
	int deathRank;
	int deathLength;
	struct {
		int move_x, move_y;
		int wallTurn;
	} inputs;
} Player_t;

enum{
	AI_WEIGHT_DEATH = -100000,
	AI_WEIGHT_KILL = 500,
	AI_WEIGHT_FRUIT = 10,
	AI_WEIGHT_WALL = -1,
	AI_VERY_BAD = -100
};

int CreatePlayer(bool isBot, const char* name);
void RemovePlayer(int id);
void ResetPlayerStats(int playerId);
void UpdatePlayers();
void LocalPlayerControls();
void UpdatePlayerRank(int playerId);

#define A_PLAYER_H
#endif
#if defined(IMP_A_PLAYER_H) || defined(__IDE__)
#include "gameapp.h"
#include "containers.h"
#include "macros.h"
#include "string.h"
#include "stdlib.h"

static const char* botNames[] = {
	"Kadum", "Rodip", "Nalbey", "Depohol", "Zilodzip", "Deyonvog", "Nudrep", "Olkyss"
};

int CreatePlayer(bool isBot, const char* name){
	int playerId;
	Player_t* player = CREATE_ENTITY(PLAYERS, &playerId);
	if(player){
		player->isBot = isBot;
		if(name){
			strncpy(player->name, name, sizeof(player->name));
		}else{
			int botNameCount = _countof(botNames);
			if(playerId < botNameCount){
				strcpy(player->name, botNames[playerId]);
			}else{
				snprintf(player->name, sizeof(player->name), "%s-%i", botNames[playerId % botNameCount], 1 + playerId / botNameCount);
			}
		}
		player->guyCount = 0;
		player->color = ColorFromHSV(35 + playerId * 100, 0.38, 0.84);
	}
	return playerId;
}

void RemovePlayer(int id){
	REMOVE_ENTITY(PLAYERS, id);
}

void ResetPlayerStats(int playerId){
	Player_t* player = GetPlayer(playerId);
	player->guyCount = 0;
}

void UpdatePlayerRank(int playerId){
	Player_t* player = GetPlayer(playerId);
	player->deathLength = 0;
	player->deathRank = 0;

	for(int iy = 0; iy < game.board.height; iy++)
	for(int ix = 0; ix < game.board.width; ix++){
		Cell_t* cell = GetCell(&game.board, ix, iy);
		if(cell->guy.bodyPart && cell->guy.playerId == playerId){
			player->deathLength++;
		}
	}

	forEachPlayer(otherPlayer, otherPlayerId){
		if(otherPlayer != player && otherPlayer->guyCount){
			player->deathRank++;
		}
	}
}

static Board_t virtualPlayfield;

static void UpdateBotAI(Player_t* player, int playerId){

	for(int iy = 0; iy < game.board.height; iy++)
	for(int ix = 0; ix < game.board.width; ix++){
		Cell_t* cell = GetCell(&game.board, ix, iy);
		if(cell->guy.bodyPart == BP_SNAKE_HEAD && cell->guy.playerId == playerId){
			int subSamples = MIN(4, 1 + game.board.waveNumber/4);
			int fwd_nx, fwd_ny;
			int bestDirection = -1;
			int bestDirectionScore;
			UnpackDirection(cell, &fwd_nx, &fwd_ny);

			for(int subSample = 0; subSample < subSamples; subSample++)
			for(int dir = 0; dir < 4; dir++){
				int dx = ddx[dir], dy = ddy[dir];
				Cell_t* frontCell;
				if(TryGetCell(&frontCell, &game.board, ix + dx, iy + dy) && IsWalkable(frontCell)){
					int maxSteps = MIN(25, 10 + 2 * game.board.waveNumber);
					bool veryBad = false;
					player->simulationScore = 0;
					player->inputs.move_x = dx;
					player->inputs.move_y = dy;
					CopyBoard(&virtualPlayfield, &game.board);
					for(int step = 0; step < maxSteps; step++){
						UpdateBoard(&virtualPlayfield);
						if((playerId + game.board.waveNumber + step + subSample) & 1){
							player->inputs.move_x = rand() % 3 - 1;
							player->inputs.move_y = rand() % 3 - 1;
						}
						veryBad = player->simulationScore <= AI_VERY_BAD;
						if(veryBad) break;
					}
					if(!veryBad && (bestDirection == -1 || player->simulationScore > bestDirectionScore || (player->simulationScore == bestDirectionScore && (rand() & 1)))){
						bestDirection = dir;
						bestDirectionScore = player->simulationScore;
					}
				}
			}
			if(bestDirection != -1){
				player->inputs.move_x = ddx[bestDirection];
				player->inputs.move_y = ddy[bestDirection];
			}

			// int distance = 0;
			// int x = ix, y = iy;
			// int prev_x, prev_y;
			
			// // cast line forward
			// forever(){
			// 	Cell_t* path;
			// 	if(TryGetCell(&path, &game.board, x -= fwd_nx, y -= fwd_ny)){
			// 		if(IsWalkable(path)){
			// 			distance++;
			// 			prev_x = x;
			// 			prev_y = y;
			// 			continue;
			// 		}
			// 	}
			// 	if(distance > 0){
					
			// 	}
			// 	break;
			// }
		}
	}
}


void UpdatePlayers(){
	forEachPlayer(player, playerId){
		if(player->isBot && player->guyCount)
			UpdateBotAI(player, playerId);
	}
}

void LocalPlayerControls(){
	Player_t* localPlayer = GetPlayer(client.playerId);
	if(localPlayer){
		int press_turn = IsKeyPressed(KEY_E) - IsKeyPressed(KEY_Q);
		int press_x = IsKeyPressed(KEY_D) - IsKeyPressed(KEY_A);
		int press_y = IsKeyPressed(KEY_S) - IsKeyPressed(KEY_W);
		if(press_x){
			int hold_y = IsKeyDown(KEY_S) - IsKeyDown(KEY_W);
			localPlayer->inputs.move_x = press_x;
			localPlayer->inputs.move_y = hold_y;
		}else if(press_y){
			int hold_x = IsKeyDown(KEY_D) - IsKeyDown(KEY_A);
			localPlayer->inputs.move_x = hold_x;
			localPlayer->inputs.move_y = press_y;
		}
		if(press_turn){
			if(press_turn == localPlayer->inputs.wallTurn){
				localPlayer->inputs.wallTurn = 0;
			}else{
				localPlayer->inputs.wallTurn = press_turn;
			}
		}
	}
}

#undef IMP_A_PLAYER_H
#endif