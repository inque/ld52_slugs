
// #define IMP_CONTROLS_H
// #include "controls.h"
#define IMP_GAMECLIENT_H
#include "gameclient.h"
#define IMP_GAMEPLAY_H
#include "gameplay.h"
#define IMP_A_BOARD_H
#include "a_board.h"
#define IMP_A_PLAYER_H
#include "a_player.h"
#define IMP_GEOMETRY_H
#include "geometry.h"
#define IMP_GAMEINTERFACE_H
#include "gameinterface.h"
// #define IMP_GRAPHICS_H
// #include "graphics.h"
// #define IMP_MENUS_H
// #include "menus.h"
#define IMP_SETTINGS_H
#include "settings.h"
// #define IMP_TRANSFORMS_H
// #include "transforms.h"
#define IMP_UTILS_H
#include "utils.h"
// #define IMP_PHYSICS_H
// #include "physics.h"

#define RAYGUI_IMPLEMENTATION
#include "raygui.h"
