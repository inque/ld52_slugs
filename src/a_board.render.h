#pragma once
#include "a_board.h"
#include "math.h"

void DrawBackground(Board_t* board){
	ClearBackground((Color){38, 40, 55, 255});
}

void DrawBoardTiles(Board_t* board){
    const Color cellColors[] = { {67, 76, 109, 255}, {47, 56, 89, 255} };

	for (int iy = 0; iy < board->height; iy++)
	for (int ix = 0; ix < board->width; ix++){
		Cell_t* cell = GetCell(board, ix, iy);
		Rectangle bounds = CellBounds(ix, iy);
		switch(cell->tile){
			case TILE_GROUND: {
                Color baseColor = cellColors[(ix+iy)&1];
				DrawRectangleRec(bounds, baseColor);
			} break;
			case TILE_FLOOD: {
				Color borderColor = MAGENTA;
				float borderWidth = 2;
				Cell_t* neighbour;
				if(TryGetCell(&neighbour, board, ix, iy - 1) && neighbour->tile != TILE_FLOOD){
					bounds = CutTop(&bounds, borderWidth);
					DrawRectangleRec(bounds, borderColor);
				}
				if(TryGetCell(&neighbour, board, ix + 1, iy) && neighbour->tile != TILE_FLOOD){
					bounds = CutRight(&bounds, borderWidth);
					DrawRectangleRec(bounds, borderColor);
				}
				if(TryGetCell(&neighbour, board, ix, iy + 1) && neighbour->tile != TILE_FLOOD){
					bounds = CutBottom(&bounds, borderWidth);
					DrawRectangleRec(bounds, borderColor);
				}
				if(TryGetCell(&neighbour, board, ix - 1, iy) && neighbour->tile != TILE_FLOOD){
					bounds = CutLeft(&bounds, borderWidth);
					DrawRectangleRec(bounds, borderColor);
				}
			} break;
		}
	}
}

void DrawBoardEntities(Board_t* board){
	for (int iy = 0; iy < board->height; iy++)
	for (int ix = 0; ix < board->width; ix++){
		Cell_t* cell = GetCell(board, ix, iy);
		if(cell->pickup){
			Rectangle bounds = CellBounds(ix, iy);
			switch(cell->pickup){
				case PICKUP_FRUIT:
                    bounds.y += powf(sinf(7 * client.time + 4 * ix + 6 * iy), 4);
					Extend(&bounds, -4);
                    DrawLineEx((Vector2){bounds.x + bounds.width/2, bounds.y}, (Vector2){bounds.x - 6 + bounds.width/2, bounds.y - 6}, 2, BROWN);
                    DrawLineEx((Vector2){bounds.x + bounds.width/2, bounds.y}, (Vector2){bounds.x + 6 + bounds.width/2, bounds.y + 6}, 4, GREEN);
					DrawRectangleRec(bounds, RED);
					break;
			}
		}
	}
}

void DrawBoardGuys(Board_t* board){
	int dx, dy;
    float jumpPhase = fabsf(cosf(PI * 2 * client.deltaFrame));
	for (int iy = 0; iy < board->height; iy++)
	for (int ix = 0; ix < board->width; ix++){
		Cell_t* cell = GetCell(board, ix, iy);
		if(cell->guy.bodyPart){
            bool isDead = cell->guy.deathTimer > 0;
			Rectangle bounds = CellBounds(ix, iy);
            Color ownerColor = MAGENTA;
            Player_t* player = GetPlayer(cell->guy.playerId);
            if(player) ownerColor = player->color;
            if(isDead){
                ownerColor.r = ownerColor.r / 2 + ownerColor.r / 4;
                ownerColor.g = ownerColor.g / 2 + ownerColor.g / 4;
                ownerColor.b = ownerColor.b / 2 + ownerColor.b / 4;
            }
			switch(cell->guy.bodyPart){
				case BP_SNAKE_TAIL:
                    for(int i = 0; i < 4; i++){
                        Cell_t* neighbour;
                        dx = ddx[i]; dy = ddy[i];
                        if(TryGetCell(&neighbour, board, ix + dx, iy + dy) && neighbour->guy.bodyPart){
                            int dx2, dy2;
                            UnpackDirection(neighbour, &dx2, &dy2);
                            if(dx2 == -dx && dy2 == -dy){
                                break;
                            }
                        }
                    }
                    if(!dx && !dy) dy = -1;
                    DrawDirectedTriangle(bounds, ownerColor, -dx, -dy);
                    break;
				case BP_SNAKE_BODY:
					DrawRectangleRec(bounds, ownerColor);
					break;
				case BP_SNAKE_HEAD: {
                    Rectangle antBounds = bounds;
                    float antScale = 0.8 + 0.3 * isDead ? 0 : jumpPhase;
                    float antOffset = 0.3f * TILE_SCREEN_SIZE;
                    Extend(&antBounds, antScale * TILE_SCREEN_SIZE / 2);
                    UnpackDirection(cell, &dx, &dy);
                    antBounds.x -= antOffset * dx;
                    antBounds.y -= antOffset * dy;
                    DrawDirectedTriangleLines(antBounds, ownerColor, 2, dx, dy, false);
					DrawRectangleRec(bounds, ownerColor);
				} break;
			}
            if(client.debugView){
                DrawRectangleLinesEx(bounds, 1, BLACK);
                UnpackDirection(cell, &dx, &dy);
                DrawRectangle(bounds.x + bounds.width * (.5f + dx / 2.0f) - 5, bounds.y + bounds.height * (.5f + dy / 2.0f) - 5, 10, 10, DARKGREEN);
            }
		}
	}
}

void DrawDirectedTriangle(Rectangle bounds, Color color, int dx, int dy){
    Vector2 halfSize = (Vector2){bounds.width / 2, bounds.height / 2};
    Vector2 center = (Vector2){bounds.x + halfSize.x, bounds.y + halfSize.y};
    Vector2 v1 = (Vector2){center.x + dx * halfSize.x, center.y + dy * halfSize.y};
    Vector2 v2 = (Vector2){center.x + (-dy - dx) * halfSize.x, center.y + (-dx - dy) * halfSize.y};
    Vector2 v3 = (Vector2){center.x + (dy - dx) * halfSize.x, center.y + (dx - dy) * halfSize.y};
    // Vector2 v2 = (Vector2){center.x - dy * halfSize.x, center.y - dx * halfSize.y};
    // Vector2 v3 = (Vector2){center.x + dy * halfSize.x, center.y + dx * halfSize.y};
    if(dx == 0)
        DrawTriangle(v3, v2, v1, color);
    else
        DrawTriangle(v1, v2, v3, color);
}

void DrawDirectedTriangleLines(Rectangle bounds, Color color, float lineWidth, int dx, int dy, bool includeBase){
    Vector2 halfSize = (Vector2){bounds.width / 2, bounds.height / 2};
    Vector2 center = (Vector2){bounds.x + halfSize.x, bounds.y + halfSize.y};
    Vector2 v1 = (Vector2){center.x + dx * halfSize.x, center.y + dy * halfSize.y};
    Vector2 v2 = (Vector2){center.x + (-dy - dx) * halfSize.x, center.y + (-dx - dy) * halfSize.y};
    Vector2 v3 = (Vector2){center.x + (dy - dx) * halfSize.x, center.y + (dx - dy) * halfSize.y};
    DrawLineEx(v1, v2, lineWidth, color);
    DrawLineEx(v3, v1, lineWidth, color);
    if(includeBase){
        DrawLineEx(v2, v3, 1, color);
    }
}
