#ifndef A_BOARD_H
#include "raylib.h"
#include "stdint.h"

enum{
	MAX_BOARD_AREA = 1024 * 1024,
	MAX_SPAWN_POINTS = 64,
	TILE_SCREEN_SIZE = 25
};

typedef enum{
	TILE_GROUND,
	TILE_FLOOD
} CellTile_e;

typedef enum{
	PICKUP_NONE,
	PICKUP_FRUIT
} Pickup_e;

typedef enum{
	BP_NONE,
	BP_SNAKE_HEAD,
	BP_SNAKE_BODY,
	BP_SNAKE_TAIL
} GuyBodyPart_e;

typedef struct{
	char tile;
	char pickup;
	struct {
		char bodyPart;
		char deathTimer;
		unsigned short playerId;
		unsigned int prev_x: 2;
		unsigned int prev_y: 2;
		unsigned int tempUpdated: 1;
	} guy;
} Cell_t;

typedef struct{
	int x, y;
	int dx, dy;
} SpawnPoint_t;

typedef struct{
	int width, height;
	int waveNumber;
	int waveTimer;
	int waveDuration;
	int spawnPickupTimer;
	int spawnPickupRate;
	uint64_t randomSeed;
	int spawnPointsUsed;
	SpawnPoint_t spawnPoints[MAX_SPAWN_POINTS];
	Cell_t cells[MAX_BOARD_AREA];
} Board_t;

void InitializeBoard(Board_t* board);
void UpdateBoard(Board_t* board);
void SpawnGuy(Board_t* board, int playerId, int x, int y, int len, int dx, int dy);
void CopyBoard(Board_t* dest, Board_t* source);

void DrawBackground(Board_t* board);
void DrawBoardTiles(Board_t* board);
void DrawBoardEntities(Board_t* board);
void DrawBoardGuys(Board_t* board);
void DrawDirectedTriangle(Rectangle bounds, Color color, int dx, int dy);
void DrawDirectedTriangleLines(Rectangle bounds, Color color, float lineWidth, int dx, int dy, bool includeBase);

// predefined directions.  U   R   D   L    UL  UR  DR  DL
static const int ddx[] = { 0,  1,  0, -1,   -1,  1,  1, -1};
static const int ddy[] = {-1,  0,  1,  0,   -1, -1,  1,  1};

static inline bool IsInBounds(Board_t* board, int x, int y){
	return x >= 0 && y >= 0 && x < board->width && y < board->height;
}

static inline int CellIndex(Board_t* board, int x, int y){
	return (x % board->width) + (y * board->width);
}

static inline Cell_t* GetCell(Board_t* board, int x, int y){
	int index = CellIndex(board, x, y);
	return &board->cells[index];
}

static inline bool TryGetCell(Cell_t** out, Board_t* board, int x, int y){
	if(!IsInBounds(board, x, y)) return false;
	*out = GetCell(board, x, y);
	return true;
}

static inline void UnpackDirection(Cell_t* cell, int* dx, int* dy){
	*dx = (int)cell->guy.prev_x - 1;
	*dy = (int)cell->guy.prev_y - 1;
}

static inline void PackDirection(Cell_t* cell, int dx, int dy){
	cell->guy.prev_x = dx + 1;
	cell->guy.prev_y = dy + 1;
}

static inline bool IsWalkable(Cell_t* cell){
	return !cell->guy.bodyPart && cell->tile == TILE_GROUND;
}

static inline Rectangle CellBounds(int x, int y){
	return (Rectangle){ x * TILE_SCREEN_SIZE, y * TILE_SCREEN_SIZE, TILE_SCREEN_SIZE, TILE_SCREEN_SIZE };
}

#define forever() while(true)
// #define forever() for(int iterationNum = 0; iterationNum < 10000; iterationNum++)

#define A_BOARD_H
#endif
#if defined(IMP_A_BOARD_H) || defined(__IDE__)
#undef IMP_A_BOARD_H

#include "gameapp.h"
#include "macros.h"
#include "geometry.h"
#include "raylib.h"
#include "string.h"
#include "stdio.h"
#include "time.h"
#include "a_board.snake.h"
#include "a_board.render.h"

void InitializeBoard(Board_t* board){
	int area;
	board->width = 50;
	board->height = 50;
	board->randomSeed = (uint64_t)time(NULL);
	board->spawnPickupRate = 4 * TICKS_PER_SEC;
	board->spawnPickupTimer = -4 * board->spawnPickupRate;
	board->waveNumber = 0;
	board->waveTimer = 0;

	area = board->width * board->height;
	memset((void*) &board->cells, 0, area * sizeof(Cell_t));

	// board->spawnPointsUsed = area / 100;
	board->spawnPointsUsed = 0;
	for(int i = 0; i < 20; i++){
		SpawnPoint_t* point = &board->spawnPoints[board->spawnPointsUsed++];
		point->x = 5 + 3 * i;
		point->y = 20;
		point->dx = 0;
		point->dy = 1;
	}

	// for (int iy = 0; iy < board->height; iy++)
	// for (int ix = 0; ix < board->width; ix++){
	// 	Cell_t* cell = GetCell(board, ix, iy);
	// 	cell->tile = TILE_GROUND;
	// }
}

void CopyBoard(Board_t* dest, Board_t* source){
	int excessCells = MAX_BOARD_AREA - (source->width * source->height);
	size_t bytes = sizeof(Board_t) - sizeof(Cell_t) * excessCells;
	memcpy((void*)dest, (void*)source, bytes);
}

void UpdateBoard(Board_t* board){
	bool isMain = board == &game.board;

	for (int iy = 0; iy < board->height; iy++)
	for (int ix = 0; ix < board->width; ix++){
		Cell_t* cell = GetCell(board, ix, iy);
		if(cell->guy.bodyPart){
			cell->guy.tempUpdated = 0;
		}
	}

	if(isMain){
		forEachPlayer(player, playerId){
			if(player->guyCount > 0){
				player->aabb[0] = INT_MAX;
				player->aabb[1] = INT_MAX;
				player->aabb[2] = INT_MIN;
				player->aabb[3] = INT_MIN;
			}
		}
	}

	for (int iy = 0; iy < board->height; iy++)
	for (int ix = 0; ix < board->width; ix++){
		Cell_t* cell = GetCell(board, ix, iy);
		if(!cell->guy.bodyPart) continue;
		if(cell->guy.tempUpdated) continue;

		if(cell->guy.deathTimer > 0){
			if(--cell->guy.deathTimer == 0){
				cell->guy.bodyPart = BP_NONE;
				if(IsWalkable(cell)){
					cell->pickup = PICKUP_FRUIT;
				}
			}
			continue;
		}

		switch(cell->guy.bodyPart){
			case BP_SNAKE_HEAD:
				UpdateSnakeHead(board, cell, ix, iy);
				break;
		}

		if(isMain){
			Player_t* player = GetPlayer(cell->guy.playerId);
			if(player){
				if(ix < player->aabb[0]) player->aabb[0] = ix;
				if(iy < player->aabb[1]) player->aabb[1] = iy;
				if(ix > player->aabb[2]) player->aabb[2] = ix;
				if(iy > player->aabb[3]) player->aabb[3] = iy;
			}
		}
	}

	if(isMain && game.state != STATE_PLAYING)
		return;

	if(--board->spawnPickupTimer < 0){
		int basePoint_x = rand32(&board->randomSeed, 0, board->width);
		int basePoint_y = rand32(&board->randomSeed, 0, board->height);
		Cell_t* cell = GetCell(board, basePoint_x, basePoint_y);
		if(IsWalkable(cell)){
			bool foundIntersection = false;
			forEachPlayer(player, playerId){
				if(player->guyCount){
					int minMargin = 2;
					if(basePoint_x >= player->aabb[0] - minMargin && basePoint_x <= player->aabb[2] + minMargin && basePoint_y >= player->aabb[1] - minMargin && basePoint_y <= player->aabb[3] + minMargin){
						foundIntersection = true;
						break;
					}
				}
			}
			if(!foundIntersection){
				cell->pickup = PICKUP_FRUIT;
				board->spawnPickupTimer += board->spawnPickupRate;
			}
		}
	}

	if(--board->waveTimer < 0){
		if(!board->waveNumber || board->waveTimer < -board->waveDuration){
			board->waveNumber++;
			board->waveTimer = 5 * TICKS_PER_SEC;
			board->waveDuration = 5 * TICKS_PER_SEC;
		}else{
			int tilesToRemove = rand32(&board->randomSeed, 1, 12);
			for(int it = 0; it < tilesToRemove; it++){
				int dir = rand32(&board->randomSeed, 0, 8);
				int dx = ddx[dir], dy = ddy[dir];
				bool isVertical = dx != 0;
				int dim = isVertical ? board->height : board->width;
				int coord = rand32(&board->randomSeed, 0, dim);
				int x = isVertical ? board->width/2 : coord;
				int y = isVertical ? coord : board->height/2;
				Cell_t *cell, *prevCell = NULL;
				while(true){
					bool outOfBounds = !TryGetCell(&cell, board, x, y);
					if(outOfBounds || cell->tile == TILE_FLOOD && prevCell && IsWalkable(prevCell)){
						prevCell->tile = TILE_FLOOD;
						prevCell->pickup = PICKUP_NONE;
						// todo: push characters
						break;
					}
					prevCell = cell;
					x += dx; y += dy;
				}
			}
		}
	}
}

void SpawnGuy(Board_t* board, int playerId, int x, int y, int len, int dx, int dy){
	Player_t* player = GetPlayer(playerId);

	for(int segNum = 0; segNum < len; segNum++){
		Cell_t* cursor = GetCell(board, x, y);
		GuyBodyPart_e bp;

		memset((void*) &cursor->guy, 0, sizeof(cursor->guy));

		if(segNum == 0){
			// head
			bp = BP_SNAKE_HEAD;
			PackDirection(cursor, -dx, -dy);
		}else if(segNum == len-1){
			// tail
			bp = BP_SNAKE_TAIL;
			PackDirection(cursor, 0, 0);
		}else{
			// body
			bp = BP_SNAKE_BODY;
			PackDirection(cursor, -dx, -dy);
		}

		cursor->guy.bodyPart = bp;
		cursor->guy.playerId = playerId;

		x -= dx;
		y -= dy;
	}

	if(player){
		player->guyCount++;
	}
}

#endif