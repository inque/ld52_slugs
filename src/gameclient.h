#ifndef GAMECLIENT_H
#include "raylib.h"
#include "containers.h"

typedef enum{
	SCREEN_MAINMENU,
	SCREEN_GAME
} ClientScreen_e;

typedef struct{
	ClientScreen_e screen;
	int width, height;
	int halfWidth, halfHeight;
	float dpi, scale;
	float time, deltaTime;
	float lastTickTime, deltaFrame;
	bool vsync;
	bool shouldClose;
	bool paused;
	int playerId;
	Camera2D cam, uiCam;
	Rectangle viewBounds;
	bool debugView;
} ClientState_t;

extern ClientState_t client;

void UpdateClient();
void ResetClient();

void CreateGameWindow();
void UpdateWindowSettings();

#define GAMECLIENT_H
#endif
#if defined(IMP_GAMECLIENT_H) || defined(__IDE__)
//==== implementation starts here ====

#include "gameapp.h"
#include "macros.h"
#include "utils.h"
#include "raymath.h"
#include "string.h"
#include "stdlib.h"
#include "stdio.h"

ClientState_t client = {
	.scale = 1
};

static void UpdateGameCamera();
static void ToggleFullscreenIfNecessary();

void UpdateClient(){
	ToggleFullscreenIfNecessary();
	#ifdef __EMSCRIPTEN__
		// client.dpi = emscripten_get_device_pixel_ratio();
		client.dpi = 1;
	#else
		client.dpi = GetWindowScaleDPI().x;
		if(!client.vsync){
			// fps limiter (experimental)
			static int lastFramerate;
			int monId = GetCurrentMonitor();
			int framerate = (GetMonitorRefreshRate(monId) * 2);
			if(framerate != lastFramerate){
				SetTargetFPS(framerate);
				lastFramerate = framerate;
			}
		}
	#endif
	client.width = GetScreenWidth();
	client.height = GetScreenHeight();
	client.halfWidth = client.width / 2;
	client.halfHeight = client.height / 2;
	client.cam.offset.x = (float)client.halfWidth;
	client.cam.offset.y = (float)client.halfHeight;
	client.time = (float)GetTime();
	client.deltaTime = GetFrameTime();
	client.deltaFrame = ((client.time - client.lastTickTime) * (float)GET_GAME_TICKRATE());
	
	if(IsKeyPressed(KEY_F4))
		client.debugView = !client.debugView;
	if(IsKeyPressed(KEY_ZERO))
		app.config.tickrate = TICKS_PER_SEC;
	if(IsKeyPressed(KEY_MINUS))
		app.config.tickrate = MAX(1, app.config.tickrate - 1);
	if(IsKeyPressed(KEY_EQUAL))
		app.config.tickrate = app.config.tickrate + 1;

	client.scale = client.dpi;
	client.cam.zoom = client.scale;
	client.uiCam.zoom = client.scale;

	UpdateGameCamera();
}

void ResetClient(){
	client.cam = (Camera2D){
		.target = { TILE_SCREEN_SIZE * game.board.width / 2, TILE_SCREEN_SIZE * game.board.height / 2 },
		.rotation = 0,
		.zoom = 1
	};
}

static void UpdateGameCamera(){
	Player_t* player = GetPlayer(client.playerId);
	float t = SmoothOver(0.9, 0.5, client.deltaTime);
	if(player && player->guyCount && player->aabb[0] != INT_MIN){
		Vector2 topLeftScreen = GetWorldToScreen2D((Vector2){ TILE_SCREEN_SIZE * player->aabb[0], TILE_SCREEN_SIZE * player->aabb[1] }, client.cam);
		Vector2 targetPos = (Vector2){
			TILE_SCREEN_SIZE * (player->aabb[0] + player->aabb[2]) / 2,
			TILE_SCREEN_SIZE * (player->aabb[1] + player->aabb[3]) / 2
		};
		client.cam.target = Vector2Lerp(client.cam.target, targetPos, t);
		if(topLeftScreen.x < 0 || topLeftScreen.y < 0){
			client.cam.zoom -= 0.5f * client.deltaTime;
		}else if(client.cam.zoom < 1){
			client.cam.zoom = MIN(1, client.cam.zoom + 0.8f * client.deltaTime);
		}
	}else{
		static float accel_x, accel_y;
		const float speed = 16;
		float move_x = IsKeyDown(KEY_D) - IsKeyDown(KEY_A);
		float move_y = IsKeyDown(KEY_S) - IsKeyDown(KEY_W);
		accel_x += client.deltaTime * move_x;
		accel_y += client.deltaTime * move_y;
		client.cam.target.x += speed * accel_x;
		client.cam.target.y += speed * accel_y;
		accel_x *= (1 - t);
		accel_y *= (1 - t);
	}
}

void CreateGameWindow(){
	bool fullscreen = (app.config.fullscreen == 1);
	bool maximized = (app.config.fullscreen == 2);
	SetConfigFlags(FLAG_WINDOW_RESIZABLE);
	SetConfigFlags(FLAG_WINDOW_HIGHDPI);
	if(fullscreen) SetConfigFlags(FLAG_FULLSCREEN_MODE);
	if(app.config.vsync){
		SetConfigFlags(FLAG_VSYNC_HINT);
		client.vsync = true;
	}
	InitWindow(app.config.resolution[0], app.config.resolution[1], GAME_TITLE);
	SetTextureFilter(GetFontDefault().texture, TEXTURE_FILTER_POINT);
	SetExitKey(0);
	SetWindowMinSize(640, 480);
	if(maximized){
		MaximizeWindow();
	}else{
		if((!fullscreen) && (app.config.windowPos[0] != -1)){
			SetWindowPosition(app.config.windowPos[0], app.config.windowPos[1]);
		}
	}

	//use SDL controller mapping file, if available
	{
		const char* gameControllerMapPath = "gamecontrollerdb.txt";
		FILE* file = fopen(gameControllerMapPath, "rb");
		if(file && (fclose(file)+1)){
			char* mappings = LoadFileText(gameControllerMapPath);
			SetGamepadMappings(mappings);
			free(mappings);
		}
	}
}

static void UpdateWindowDimensionsInsideOfSettings(bool updateSize, bool updatePos){
	if(updateSize){
		app.config.resolution[0] = GetScreenWidth();
		app.config.resolution[1] = GetScreenHeight();
	}
	if(updatePos){
		Vector2 windowPos = GetWindowPosition();
		app.config.windowPos[0] = (int)windowPos.x;
		app.config.windowPos[1] = (int)windowPos.y;
	}
}

void UpdateWindowSettings(){
	bool fullscreen = IsWindowFullscreen();
	bool maximized = IsWindowMaximized();
	if(fullscreen){
		app.config.fullscreen = 1;
		UpdateWindowDimensionsInsideOfSettings(true, false);
	}else if(maximized){
		app.config.fullscreen = 2;
	}else{
		app.config.fullscreen = 0;
		UpdateWindowDimensionsInsideOfSettings(true, true);
	}
}

static void ToggleFullscreenIfNecessary(){
	bool alt = IsKeyDown(KEY_LEFT_ALT) || IsKeyDown(KEY_RIGHT_ALT);
	bool enter = IsKeyPressed(KEY_ENTER);
	if(alt && enter){
		int monId = GetCurrentMonitor();
		int monRes[] = {GetMonitorWidth(monId), GetMonitorHeight(monId)};
		if(!IsWindowFullscreen()){
			//going fullscreen
			UpdateWindowDimensionsInsideOfSettings(true, true);
			SetWindowSize(monRes[0], monRes[1]);
			ToggleFullscreen();
		}else{
			//going windowed
			ToggleFullscreen();
			if(!memcmp(&monRes, &app.config.resolution, sizeof(monRes))){
				//no windowed resolution saved (app started in fullscreen)
				app.config.resolution[0] = MAX(640, app.config.resolution[0] * 3 / 4 - 250);
				app.config.resolution[1] = MAX(480, app.config.resolution[1] * 3 / 4 - 200);
			}
			SetWindowSize(app.config.resolution[0], app.config.resolution[1]);
			SetWindowPosition(app.config.windowPos[0], app.config.windowPos[1]);
		}
	}
}

#undef IMP_GAMECLIENT_H
#endif
