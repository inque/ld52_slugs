
#ifndef GEOMETRY_H
#include "utils.h"
#include "raylib.h"
#include "raymath.h"
#include "stdbool.h"

//## Triangulation

#define TRIANGULATE_MAX 32

typedef struct{
	int triCount;
	int indices[TRIANGULATE_MAX][3];
} Triangulation2dOutput_t;

void Triangulate2d_EarClip(int vertCount, const Vector2 *verts, const char* isInnerVert, Triangulation2dOutput_t* out);
void Triangulate2d_Delaunay(int vertCount, const Vector2 *verts, const char* isInnerVert, Triangulation2dOutput_t* out);
void Tesselate2d_Delaunay(int vertCount, const Vector2 *verts, const char* isInnerVert, Triangulation2dOutput_t* state);
bool IsConvex2d(int vertCount, Vector2* verts);

//## Rect utils

// inspired by: https://halt.software/dead-simple-layouts/
Rectangle CutLeft(Rectangle* rect, float a);
Rectangle CutRight(Rectangle* rect, float a);
Rectangle CutTop(Rectangle* rect, float a);
Rectangle CutBottom(Rectangle* rect, float a);
void Extend(Rectangle* rect, float a);
Rectangle FsRect();
bool IsMouseInRect(Rectangle bounds);

#define GEOMETRY_H
#endif
#if defined(IMP_GEOMETRY_H) || defined(__IDE__)
//==== implementation starts here ====

#include "gameapp.h"

//## Triangulation

static bool IsTriangleClockWise2d(Vector2 p1, Vector2 p2, Vector2 p3){
	float a = p2.x * p1.y + p3.x * p2.y + p1.x * p3.y;
	float b = p1.x * p2.y + p2.x * p3.y + p3.x * p1.y;
	return (a > b);
}

static bool IsPointInTriangle2d(Vector2 p, Vector2* t){
	for(int i=0; i<3; i++)
		if(IsTriangleClockWise2d(t[i], t[(i + 1) % 3], p)) return false;
	return true;
}

static bool Ear_Q(int i, int j, int k, int vertCount, const Vector2* verts){
	Vector2 t[3];

	t[0].x = verts[i].x;
	t[0].y = verts[i].y;

	t[1].x = verts[j].x;
	t[1].y = verts[j].y;

	t[2].x = verts[k].x;
	t[2].y = verts[k].y;

	if(IsTriangleClockWise2d(t[0], t[1], t[2])) return false;

	for(int m=0; m < vertCount; m++){
		if((m != i) && (m != j) && (m != k))
			if(IsPointInTriangle2d(verts[m], (Vector2*) &t)) return false;
	}
	return true;
}

void Triangulate2d_EarClip(int vertCount, const Vector2 *verts, const char* isInnerVert, Triangulation2dOutput_t* out){
	// left/right neighbor indices
	int l[TRIANGULATE_MAX], r[TRIANGULATE_MAX];
	for(int i=0; i < vertCount; i++){
		l[i] = ((i-1) + vertCount) % vertCount;
		r[i] = ((i+1) + vertCount) % vertCount;
		if (isInnerVert) {
			while(isInnerVert[l[i]]) l[i] = (l[i]-1) % vertCount;
			while(isInnerVert[r[i]]) r[i] = (r[i]+1) % vertCount;
		}
	}
	out->triCount = 0;
	// int i = vertCount-1;
	int i = 0;
	int iterationCounter = 0;
	while(out->triCount < (vertCount - 2)){
		i = r[i];
		// if (isInnerVert && (isInnerVert[i] || isInnerVert[l[i]])) continue;
		if( (iterationCounter++) > vertCount ) return;
		if (isInnerVert && isInnerVert[i]) continue;
		if(Ear_Q(l[i], i, r[i], vertCount, verts)){
			int* newTriangle = &out->indices[out->triCount++][0];
			newTriangle[0] = l[i];
			newTriangle[1] = i;
			newTriangle[2] = r[i];
			if(out->triCount == TRIANGULATE_MAX) return;
			// do{
				l[ r[i] ] = l[i];
			// }while(!isInnerVert || !isInnerVert[l[i]]);
			
			// do{
				r[ l[i] ] = r[i];
			// }while(!isInnerVert || !isInnerVert[r[i]]);

		}
	}
}

void Triangulate2d_Delaunay(int vertCount, const Vector2 *verts, const char* isInnerVert, Triangulation2dOutput_t* out){
	Rectangle bbox = {0};
	float right, bottom;
	const float supraMagic = 3.1416 / 6 + 0.1;
	float supra_x, supra_w;
	Vector2 supraVerts[3];
	int outTriCount;
	int outIndices[TRIANGULATE_MAX][3];
	struct {
		bool isBad;
		bool haveCircum;
		Vector2 circumCenter;
		float circumRadiusSqr;
	} triangleState[TRIANGULATE_MAX] = { {.isBad = true} };

	// printf("<g>\n");
	// for (int vertId = 0; vertId < vertCount; vertId++)
	// 	printf("\t<rect x=\"%f\" y=\"%f\" width=\"10\" height=\"10\" style=\"fill:white; stroke:black;\" />\n", verts[vertId].x-5, verts[vertId].y-5);
	// printf("</g>\n");

	// calculate the bounding box
	for (int vertId = 0; vertId < vertCount; vertId++) {
		Vector2 vert = verts[vertId];
		if (vertId == 0) {
			bbox.x = vert.x;
			bbox.y = vert.y;
			continue;
		}
		right = bbox.x + bbox.width;
		bottom = bbox.y + bbox.height;
		if (vert.x < bbox.x) { bbox.width  += (bbox.x - vert.x); bbox.x = vert.x; }
		if (vert.y < bbox.y) { bbox.height += (bbox.y - vert.y); bbox.y = vert.y; }
		if (vert.x > right) bbox.width += (vert.x - right);
		if (vert.y > bottom) bbox.height += (vert.y - bottom);
	}
	// printf("<rect x=\"%f\" y=\"%f\" width=\"%f\" height=\"%f\" style=\"fill:none; stroke:#bbb;\" />\n", bbox.x, bbox.y, bbox.width, bbox.height);

	// construct a virtual triangle that covers all of the vertices
	supra_x = bbox.x + bbox.width / 2;
	supra_w = (bbox.width + bbox.height) * supraMagic;
	bottom = bbox.y + bbox.height + 0.1;
	supraVerts[0] = (Vector2){ supra_x - supra_w, bottom };
	supraVerts[1] = (Vector2){ supra_x + supra_w, bottom };
	supraVerts[2] = (Vector2){ supra_x, bbox.y - bbox.width - 0.1 };
	outTriCount = 1;
	outIndices[0][0] = -1;
	outIndices[0][1] = -2;
	outIndices[0][2] = -3;
	// printf(
	// 	"<g id=\"supra\">\n"
	// 	"\t<line x1=\"%f\" y1=\"%f\" x2=\"%f\" y2=\"%f\" style=\"stroke:#777\" />\n"
	// 	"\t<line x1=\"%f\" y1=\"%f\" x2=\"%f\" y2=\"%f\" style=\"stroke:#777\" />\n"
	// 	"\t<line x1=\"%f\" y1=\"%f\" x2=\"%f\" y2=\"%f\" style=\"stroke:#777\" />\n"
	// 	"</g>\n",
	// 	supraVerts[0].x, supraVerts[0].y, supraVerts[1].x, supraVerts[1].y,
	// 	supraVerts[1].x, supraVerts[1].y, supraVerts[2].x, supraVerts[2].y,
	// 	supraVerts[2].x, supraVerts[2].y, supraVerts[0].x, supraVerts[0].y
	// );
	
	for (int vertId = 0; vertId < vertCount; vertId++) {
		Vector2 vert = verts[vertId];
	// for (int vertId = -3; vertId < vertCount; vertId++) {
	// 	Vector2 vert = vertId < 0 ? supraVerts[-vertId-1] : verts[vertId];
		int currentTriCount = outTriCount;
		
		// printf("<g id=\"vert_%i\">\n", vertId);
		// printf("\t<rect x=\"%f\" y=\"%f\" width=\"10\" height=\"10\" />\n", vert.x-5, vert.y-5);

		// mark triangles that the vertex lies inside circumference of
		if (outTriCount > 1) {
			for (int tri_id = 0; tri_id < outTriCount; tri_id++) {
				if (!triangleState[tri_id].haveCircum) {
					// find the circle that passes all 3 vertices of the triangle
					int a_ind = outIndices[tri_id][0];
					int b_ind = outIndices[tri_id][1];
					int c_ind = outIndices[tri_id][2];

					// bool haveSupra = a_ind < 0 || b_ind < 0 || c_ind < 0;
					// bool haveInner = (a_ind >= 0 && isInnerVert[a_ind]) || (b_ind >= 0 && isInnerVert[b_ind]) && (c_ind >= 0 && isInnerVert[c_ind]);
					// if (haveSupra && haveInner)
					// 	continue;

					Vector2 a = a_ind < 0 ? supraVerts[-a_ind-1] : verts[a_ind];
					Vector2 b = b_ind < 0 ? supraVerts[-b_ind-1] : verts[b_ind];
					Vector2 c = c_ind < 0 ? supraVerts[-c_ind-1] : verts[c_ind];
					// float a_mag = Vector2LengthSqr(a);
					// float b_mag = Vector2LengthSqr(b);
					// float c_mag = Vector2LengthSqr(c);
					// float d = (( a.x * (b.y - c.y) + b.x * (c.y - a.y) + c.x * (a.y - b.y) )) * 2;
					// triangleState[tri_id].circumCenter = (Vector2){
					// 	(a_mag * (b.y - c.y) + b_mag * (c.y - a.y) + c_mag * (a.y - b.y)) / d,
					// 	(a_mag * (b.x - c.x) + b_mag * (c.x - a.x) + c_mag * (a.x - b.x)) / d
					// };

					Vector2 SqrA = (Vector2){a.x * a.x, a.y * a.y};
					Vector2 SqrB = (Vector2){b.x * b.x, b.y * b.y};
					Vector2 SqrC = (Vector2){c.x * c.x, c.y * c.y};
					float d = (a.x * (b.y - c.y) + b.x * (c.y - a.y) + c.x * (a.y - b.y)) * 2.0f;
					triangleState[tri_id].circumCenter = (Vector2){
						((SqrA.x + SqrA.y) * (b.y - c.y) + (SqrB.x + SqrB.y) * (c.y - a.y) + (SqrC.x + SqrC.y) * (a.y - b.y)) / d,
						((SqrA.x + SqrA.y) * (c.x - b.x) + (SqrB.x + SqrB.y) * (a.x - c.x) + (SqrC.x + SqrC.y) * (b.x - a.x)) / d
					};

					triangleState[tri_id].circumRadiusSqr = Vector2DistanceSqr(a, triangleState[tri_id].circumCenter);
					triangleState[tri_id].haveCircum = true;

					// printf("checking circum between (%f %f), (%f %f) and (%f %f).\n", a.x, a.y, b.x, b.y, c.x, c.y);
					// printf("res: (%f %f). radius: %f. d: %f\n", triangleState[tri_id].circumCenter.x, triangleState[tri_id].circumCenter.y, sqrtf(triangleState[tri_id].circumRadiusSqr), d);
				}
				if (Vector2DistanceSqr(vert, triangleState[tri_id].circumCenter) < triangleState[tri_id].circumRadiusSqr) {
					triangleState[tri_id].isBad = true;
					// printf("triangle %i is bad. vert %i.\n", tri_id, vertId);
				}
				// printf(
				// 	"\t<circle cx=\"%f\" cy=\"%f\" r=\"%f\" style=\"stroke:%s; fill:none;\" />\n",
				// 	triangleState[tri_id].circumCenter.x, triangleState[tri_id].circumCenter.y,
				// 	sqrtf(triangleState[tri_id].circumRadiusSqr), 
				// 	(triangleState[tri_id].isBad ? "pink" : "green")
				// );
			}
		}

		// tesselate the polygon formed by 'bad' triangles
		for (int tri_id = 0; tri_id < currentTriCount; tri_id++) {
			if (triangleState[tri_id].isBad) {
				// bool skipEdge[3] = {0};
				int a = 0, b = 1;

				for (int edgeNum = 0; edgeNum < 3; edgeNum++) {
					bool skipEdge = false;

					for (int otherTri_id = 0; otherTri_id < currentTriCount; otherTri_id++) {
						if (otherTri_id != tri_id && triangleState[otherTri_id].isBad) {
							int *current = &outIndices[tri_id][0];
							int *other   = &outIndices[otherTri_id][0];
							bool have_a = (other[0] == current[a] || other[1] == current[a] || other[2] == current[a]);
							bool have_b = (other[0] == current[b] || other[1] == current[b] || other[2] == current[b]);
							if (have_a && have_b) {
								// printf("skipping. vert %i. edge %i (%i %i). [%i] (%i %i %i) vs [%i] (%i %i %i)\n", vertId, edgeNum, current[a], current[b], tri_id, current[0], current[1], current[2], otherTri_id, other[0], other[1], other[2]);
								skipEdge = true;
								break;
							}
						}
					}

					if (!skipEdge) {
						outIndices[outTriCount][0] = outIndices[tri_id][a];
						outIndices[outTriCount][1] = outIndices[tri_id][b];
						outIndices[outTriCount][2] = vertId;
						// {
						// 	int a_ind = outIndices[outTriCount][0];
						// 	int b_ind = outIndices[outTriCount][1];
						// 	int c_ind = outIndices[outTriCount][2];
						// 	Vector2 a = a_ind < 0 ? supraVerts[-a_ind-1] : verts[a_ind];
						// 	Vector2 b = b_ind < 0 ? supraVerts[-b_ind-1] : verts[b_ind];
						// 	Vector2 c = c_ind < 0 ? supraVerts[-c_ind-1] : verts[c_ind];
						// 	printf(
						// 		"\t<path d=\"M %f %f L %f %f L %f %f Z\" style=\"stroke:red; fill:none;\" data=\"%i %i %i\" />\n",
						// 		a.x, a.y, b.x, b.y, c.x, c.y,
						// 		a_ind, b_ind, c_ind
						// 	);
						// }

						if (++outTriCount == TRIANGULATE_MAX)
							goto breakIfOutOfSpace;
					}
					a++;
					if (++b == 3) b = 0;
				}
			}
		}
		breakIfOutOfSpace: {}

		// remove bad triangles
		for (int tri_id = 0; tri_id < currentTriCount; tri_id++) {
			if (triangleState[tri_id].isBad) {
				int newCount = outTriCount - 1;
				int blocksToMove = newCount - tri_id;
				if (blocksToMove > 0) {
					memmove(&outIndices[tri_id][0], &outIndices[tri_id + 1][0], blocksToMove * 3 * sizeof(int));
					memmove(&triangleState[tri_id], &triangleState[tri_id + 1], blocksToMove * sizeof(triangleState[0]));
					tri_id--;
				} else {
					triangleState[tri_id].isBad = false;
					triangleState[tri_id].haveCircum = false;
				}
				outTriCount = newCount;
			}
		}

		// printf("</g>\n");
	}

	// return the filtered indices
	out->triCount = 0;
	// printf("<g id=\"result\">\n");
	for (int tri_id = 0; tri_id < outTriCount; tri_id++) {
		int* src = &outIndices[tri_id][0];
		if (src[0] < 0 || src[1] < 0 || src[2] < 0) {
			// skip virtual (related to initial supra triangle)
			continue;
		// } else if ( (isInnerVert[src[0]] + isInnerVert[src[1]] + isInnerVert[src[2]]) > 1 ) {
		// 	// skip external (as to make it concave)
		// 	continue;
		} else {
			int index = out->triCount++;
			int* dst = &out->indices[index][0];
			memcpy(dst, src, 3 * sizeof(int));
			if (IsTriangleClockWise2d(verts[src[0]], verts[src[1]], verts[src[2]])) {
				int temp = out->indices[index][0];
				out->indices[index][0] = out->indices[index][2];
				out->indices[index][2] = temp;
			}
			// printf(
			// 	"\t<path d=\"M %f %f L %f %f L %f %f Z\" style=\"stroke:green; fill:none;\" />\n",
			// 	verts[out->indices[index][0]].x, verts[out->indices[index][0]].y,
			// 	verts[out->indices[index][1]].x, verts[out->indices[index][1]].y,
			// 	verts[out->indices[index][2]].x, verts[out->indices[index][2]].y
			// );
		}
	}
	// printf("</g>\n");

	// // fix the holes
	// while (out->triCount < TRIANGULATE_MAX){
	// 	for(int vert_a = 0; vert_a < vertCount-1; vert_a++){
	// 		if(isInnerVert[vert_a]) continue;

	// 		for(int offset = 1; offset < vertCount; offset++){
	// 			int vert_b = (vert_a + offset) % vertCount;
	// 			if(isInnerVert[vert_b]) continue;
				
	// 			for(int tri_id = 0; tri_id < out->triCount; tri_id++){
	// 				int* tri = &out->indices[tri_id * 3];
	// 				for(int i=0; i < 3; i++)
	// 					if(tri[i] == vert_a && tri[(i + 1) % 3] == vert_b)
	// 						goto continueHoleFix;
	// 			}
	// 			// found

	// 			goto continueHoleFix;
	// 		}
	// 	}
	// 	break;
	// 	continueHoleFix: {}
	// }
}

void Tesselate2d_Delaunay(int vertCount, const Vector2 *verts, const char* isInnerVert, Triangulation2dOutput_t* out){
	int outTriCount;
	int outIndices[TRIANGULATE_MAX][3];
	struct {
		bool isBad;
		bool haveCircum;
		Vector2 circumCenter;
		float circumRadiusSqr;
	// } triangleState[TRIANGULATE_MAX] = { {.isBad = true} };
	} triangleState[TRIANGULATE_MAX] = {0};

	outTriCount = out->triCount;
	printf("given %i triangles.\n", outTriCount);
	for (int i = 0; i < outTriCount; i++)
		for (int j = 0; j < 3; j++){
			outIndices[i][j] = out->indices[i][j];
			// triangleState[i].isBad = true;
		}

	
	for (int vertId = vertCount-1; vertId >= 0; vertId--) {
		Vector2 vert = verts[vertId];
		int currentTriCount = outTriCount;
		// if (!isInnerVert[vertId]) continue;
		
		// mark triangles that the vertex lies inside circumference of
		if (outTriCount > 1) {
			for (int tri_id = 0; tri_id < outTriCount; tri_id++) {
				if (!triangleState[tri_id].haveCircum) {
					// find the circle that passes all 3 vertices of the triangle
					int a_ind = outIndices[tri_id][0];
					int b_ind = outIndices[tri_id][1];
					int c_ind = outIndices[tri_id][2];
					Vector2 a = verts[a_ind];
					Vector2 b = verts[b_ind];
					Vector2 c = verts[c_ind];
					
					Vector2 SqrA = (Vector2){a.x * a.x, a.y * a.y};
					Vector2 SqrB = (Vector2){b.x * b.x, b.y * b.y};
					Vector2 SqrC = (Vector2){c.x * c.x, c.y * c.y};
					float d = (a.x * (b.y - c.y) + b.x * (c.y - a.y) + c.x * (a.y - b.y)) * 2.0f;
					triangleState[tri_id].circumCenter = (Vector2){
						((SqrA.x + SqrA.y) * (b.y - c.y) + (SqrB.x + SqrB.y) * (c.y - a.y) + (SqrC.x + SqrC.y) * (a.y - b.y)) / d,
						((SqrA.x + SqrA.y) * (c.x - b.x) + (SqrB.x + SqrB.y) * (a.x - c.x) + (SqrC.x + SqrC.y) * (b.x - a.x)) / d
					};

					triangleState[tri_id].circumRadiusSqr = Vector2DistanceSqr(a, triangleState[tri_id].circumCenter);
					triangleState[tri_id].haveCircum = true;

					// printf("checking circum between (%f %f), (%f %f) and (%f %f).\n", a.x, a.y, b.x, b.y, c.x, c.y);
					// printf("res: (%f %f). radius: %f. d: %f\n", triangleState[tri_id].circumCenter.x, triangleState[tri_id].circumCenter.y, sqrtf(triangleState[tri_id].circumRadiusSqr), d);
				}
				if (Vector2DistanceSqr(vert, triangleState[tri_id].circumCenter) < triangleState[tri_id].circumRadiusSqr) {
					triangleState[tri_id].isBad = true;
					// printf("triangle %i is bad. vert %i.\n", tri_id, vertId);
				}
				// printf(
				// 	"\t<circle cx=\"%f\" cy=\"%f\" r=\"%f\" style=\"stroke:%s; fill:none;\" />\n",
				// 	triangleState[tri_id].circumCenter.x, triangleState[tri_id].circumCenter.y,
				// 	sqrtf(triangleState[tri_id].circumRadiusSqr), 
				// 	(triangleState[tri_id].isBad ? "pink" : "green")
				// );
			}
		}

		// tesselate the polygon formed by 'bad' triangles
		for (int tri_id = 0; tri_id < currentTriCount; tri_id++) {
			if (triangleState[tri_id].isBad) {
				// bool skipEdge[3] = {0};
				int a = 0, b = 1;

				for (int edgeNum = 0; edgeNum < 3; edgeNum++) {
					bool skipEdge = false;
					int *current = &outIndices[tri_id][0];

					if (vertId == current[a] || vertId == current[b])
						skipEdge = true;
					else

					// if (!isInnerVert[current[a]] && !isInnerVert[current[b]]){
					// 	skipEdge = true;
					// }else
					{
						for (int otherTri_id = 0; otherTri_id < currentTriCount; otherTri_id++) {
							if (otherTri_id != tri_id && triangleState[otherTri_id].isBad) {
								int *other   = &outIndices[otherTri_id][0];
								bool have_a = (other[0] == current[a] || other[1] == current[a] || other[2] == current[a]);
								bool have_b = (other[0] == current[b] || other[1] == current[b] || other[2] == current[b]);
								if (have_a && have_b) {
									// printf("skipping. vert %i. edge %i (%i %i). [%i] (%i %i %i) vs [%i] (%i %i %i)\n", vertId, edgeNum, current[a], current[b], tri_id, current[0], current[1], current[2], otherTri_id, other[0], other[1], other[2]);
									skipEdge = true;
									break;
								}
							}
						}
					}

					if (!skipEdge) {
						printf("tesselating tri %i [%i %i %i] edge %i --> [%i %i %i]\n", tri_id, current[0], current[1], current[2], edgeNum, current[a], current[b], vertId);
						outIndices[outTriCount][0] = current[a];
						outIndices[outTriCount][1] = current[b];
						outIndices[outTriCount][2] = vertId;
						// {
						// 	int a_ind = outIndices[outTriCount][0];
						// 	int b_ind = outIndices[outTriCount][1];
						// 	int c_ind = outIndices[outTriCount][2];
						// 	Vector2 a = a_ind < 0 ? supraVerts[-a_ind-1] : verts[a_ind];
						// 	Vector2 b = b_ind < 0 ? supraVerts[-b_ind-1] : verts[b_ind];
						// 	Vector2 c = c_ind < 0 ? supraVerts[-c_ind-1] : verts[c_ind];
						// 	printf(
						// 		"\t<path d=\"M %f %f L %f %f L %f %f Z\" style=\"stroke:red; fill:none;\" data=\"%i %i %i\" />\n",
						// 		a.x, a.y, b.x, b.y, c.x, c.y,
						// 		a_ind, b_ind, c_ind
						// 	);
						// }

						if (++outTriCount == TRIANGULATE_MAX)
							goto breakIfOutOfSpace;
					}
					a++;
					if (++b == 3) b = 0;
				}
			}
		}
		breakIfOutOfSpace: {}

		// remove bad triangles
		for (int tri_id = 0; tri_id < currentTriCount; tri_id++) {
			if (triangleState[tri_id].isBad) {
				printf("removing bad triangle %i [%i %i %i]\n", tri_id, outIndices[tri_id][0], outIndices[tri_id][1], outIndices[tri_id][2]);
				int newCount = outTriCount - 1;
				int blocksToMove = newCount - tri_id;
				if (blocksToMove > 0) {
					memmove(&outIndices[tri_id][0], &outIndices[tri_id + 1][0], blocksToMove * 3 * sizeof(int));
					memmove(&triangleState[tri_id], &triangleState[tri_id + 1], blocksToMove * sizeof(triangleState[0]));
					tri_id--;
				} else {
					triangleState[tri_id].isBad = false;
					triangleState[tri_id].haveCircum = false;
				}
				outTriCount = newCount;
			}
		}

		// printf("</g>\n");
	}

	// return the filtered indices
	out->triCount = 0;
	// printf("<g id=\"result\">\n");
	for (int tri_id = 0; tri_id < outTriCount; tri_id++) {
		int* src = &outIndices[tri_id][0];
		int index = out->triCount++;
		int* dst = &out->indices[index][0];
		memcpy(dst, src, 3 * sizeof(int));
		if (IsTriangleClockWise2d(verts[src[0]], verts[src[1]], verts[src[2]])) {
			int temp = out->indices[index][0];
			out->indices[index][0] = out->indices[index][2];
			out->indices[index][2] = temp;
		}
		printf("adding triangle %i. [%i %i %i] \n", index, dst[0], dst[1], dst[2]);
		// printf(
		// 	"\t<path d=\"M %f %f L %f %f L %f %f Z\" style=\"stroke:green; fill:none;\" />\n",
		// 	verts[out->indices[index][0]].x, verts[out->indices[index][0]].y,
		// 	verts[out->indices[index][1]].x, verts[out->indices[index][1]].y,
		// 	verts[out->indices[index][2]].x, verts[out->indices[index][2]].y
		// );
}
}

bool IsConvex2d(int vertCount, Vector2* verts){
	for(int i=0; i < vertCount; i++) {
		if(!IsTriangleClockWise2d(verts[i], verts[(i + 1) % vertCount], verts[(i + 2) % vertCount])) return false;
	}
	return true;
}


//## Rect utils

Rectangle CutLeft(Rectangle* rect, float a){
	float x = rect->x;
	rect->x += a;
	rect->width -= a;
	return (Rectangle){x, rect->y, a, rect->height};
}

Rectangle CutRight(Rectangle* rect, float a){
	float width = rect->width;
	rect->width -= a;
	return (Rectangle){rect->x + width - a, rect->y, a, rect->height};
}

Rectangle CutTop(Rectangle* rect, float a){
	float y = rect->y;
	rect->y += a;
	rect->height -= a;
	return (Rectangle){rect->x, y, rect->width, a};
}

Rectangle CutBottom(Rectangle* rect, float a){
	float height = rect->height;
	rect->height -= a;
	return (Rectangle){rect->x, rect->y + height - a, rect->width, a};
}

void Extend(Rectangle* rect, float a){
	rect->x -= a;
	rect->y -= a;
	rect->width += a*2;
	rect->height += a*2;
}

Rectangle FsRect(){ return (Rectangle){0, 0, client.width, client.height}; }

bool IsMouseInRect(Rectangle bounds){
	Vector2 mousePos = GetMousePosition();
	if(mousePos.x < bounds.x) return false;
	if(mousePos.y < bounds.y) return false;
	if(mousePos.x > (bounds.x + bounds.width)) return false;
	if(mousePos.y > (bounds.y + bounds.height)) return false;
	return true;
}

#undef IMP_GEOMETRY_H
#endif
