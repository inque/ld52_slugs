#pragma once
#include "a_board.h"

static void UpdateSnakeHead(Board_t* board, Cell_t* cell, int ix, int iy){
	bool isMain = board == &game.board;
	int fwd_x = -((int)cell->guy.prev_x - 1);
	int fwd_y = -((int)cell->guy.prev_y - 1);

	// determine preferred move direction
	int pref_dx = fwd_x;
	int pref_dy = fwd_y;
	Player_t* player = GetPlayer(cell->guy.playerId);
	int input_x = player->inputs.move_x;
	int input_y = player->inputs.move_y;
	if(!input_x || !input_y){
		if(input_x && input_y){
			if(fwd_x != 0){
				input_x = 0;
			}else{
				input_y = 0;
			}
		}
		if(input_x != -fwd_x && input_y != -fwd_y){
			pref_dx = input_x;
			pref_dy = input_y;
		}
	}
	if(isMain){
		player->inputs.move_x = 0;
		player->inputs.move_y = 0;
	}

	// determine next move
	Cell_t* frontCell;
	int moveDirection[2];
	bool anyMoveFound = false;

	for(int i = -1; i < 4; i++){
		int dx, dy;
		if(i >= 0){
			dx = ddx[i];
			dy = ddy[i];
		}else{
			int wallTurn = player->inputs.wallTurn;
			if(!wallTurn) continue;
			dx = -fwd_y * wallTurn;
			dy = fwd_x * wallTurn;
		}
		Cell_t* targetCell;
		if(TryGetCell(&targetCell, board, ix + dx, iy + dy) && IsWalkable(targetCell)){
			bool isPreferred = (dx == pref_dx && dy == pref_dy);
			if(!anyMoveFound || isPreferred){
				moveDirection[0] = dx;
				moveDirection[1] = dy;
				frontCell = targetCell;
				anyMoveFound = true;
			}
			if(isPreferred)
				break;
		}
	}
	if(isMain && anyMoveFound && (moveDirection[0] != fwd_x || moveDirection[1] != fwd_y)){
		player->inputs.wallTurn = 0;
	}

	if(!anyMoveFound){
		// nowhere to move, die
		if(!isMain || game.state != STATE_ENDED){
			Player_t* player = GetPlayer(cell->guy.playerId);
			Cell_t* cursor = cell;
			int timer = 1 * TICKS_PER_SEC;
			int timerInc = 1;
			int x = ix, y = iy;
			forever(){
				int dx, dy;
				UnpackDirection(cursor, &dx, &dy);
				cursor->guy.deathTimer = timer;
				timer = MIN(255, timer + timerInc);
				if(!dx && !dy) break;
				if(!TryGetCell(&cursor, board, x += dx, y += dy)) break;
				if(!cursor->guy.bodyPart || cursor->guy.playerId != cell->guy.playerId || cursor->guy.deathTimer) break;
			}
			if(player){
				if(isMain){
					player->guyCount--;
					UpdatePlayerRank(cell->guy.playerId);
				}else{
					player->simulationScore += AI_WEIGHT_DEATH;
					forEachPlayer(otherPlayer, otherPlayerId){
						if(otherPlayer != player){
							otherPlayer->simulationScore += AI_WEIGHT_KILL;
						}
					}
					if(moveDirection[0] != pref_dx || moveDirection[1] != pref_dy)
						player->simulationScore += AI_WEIGHT_WALL;
				}
			}
		}
	}else if(frontCell->pickup == PICKUP_FRUIT){
		frontCell->pickup = PICKUP_NONE;
		cell->guy.tempUpdated = 1;
		frontCell->guy = cell->guy;
		PackDirection(frontCell, -moveDirection[0], -moveDirection[1]);
		cell->guy.bodyPart = BP_SNAKE_BODY;
		if(!isMain){
			Player_t* player = GetPlayer(frontCell->guy.playerId);
			if(player){
				player->simulationScore += AI_WEIGHT_FRUIT;
			}
		}
	}else{
		// move the whole body by 1 cell head to tail
		Cell_t* backCell = cell;
		backCell->guy.tempUpdated = 1;
		int x = ix, y = iy;
		int dx = -moveDirection[0], dy = -moveDirection[1];
		forever(){
			int ndx, ndy;
			frontCell->guy = backCell->guy;
			UnpackDirection(frontCell, &ndx, &ndy);
			if(ndx || ndy){
				PackDirection(frontCell, dx, dy);
			}
			dx = ndx, dy = ndy;
			frontCell = backCell;
			x += ndx; y += ndy;
			if((!ndx && !ndy) || !TryGetCell(&backCell, board, x, y) || !backCell->guy.bodyPart || backCell->guy.playerId != frontCell->guy.playerId){
				frontCell->guy.bodyPart = BP_NONE;
				break;
			}
		}
	}
}
