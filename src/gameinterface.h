#ifndef GAMEINTERFACE_H
#define GAMEINTERFACE_H

void DrawOverlay();
void DrawHUD();

#endif
#if defined(IMP_GAMEINTERFACE_H) || defined(__IDE__)
#undef IMP_GAMEINTERFACE_H

#include "gameapp.h"
#include "stdio.h"
#include "math.h"

static float deathTime;
static float roundEndTime;

#define CLEAR ((Color){0})

void DrawOverlay(){
	Player_t* player = GetPlayer(client.playerId);
	if(player && player->guyCount){
		
		if(player->inputs.wallTurn){
			for(int iy = 0; iy < game.board.height; iy++)
			for(int ix = 0; ix < game.board.width; ix++){
				Cell_t* cell = GetCell(&game.board, ix, iy);
				if(cell->guy.bodyPart == BP_SNAKE_HEAD && cell->guy.playerId == client.playerId){
					int distance = 0;
					int x = ix, y = iy;
					int fwd_nx, fwd_ny;
					UnpackDirection(cell, &fwd_nx, &fwd_ny);
					
					forever(){
						Cell_t* path;
						if(TryGetCell(&path, &game.board, x -= fwd_nx, y -= fwd_ny)){
							if(IsWalkable(path)){
								distance++;
								continue;
							}
						}
						if(distance > 0){
							int turn = player->inputs.wallTurn;
							Rectangle bounds = CellBounds(x + fwd_nx, y + fwd_ny);
							Extend(&bounds, -4);
							DrawDirectedTriangle(bounds, Fade(RED, 0.5), fwd_ny * turn, -fwd_nx * turn);
						}
						break;
					}
				}
			}
		}
	}
}

static void DrawPlayer(Player_t* player, int playerId, Vector2 pos){
	const float segmentSize = 15;
	Vector2 segmentAt = pos;
	int length = (player->deathLength == 0) ? 6 : player->deathLength;
	bool isAlive = player->guyCount;
	char text[128];
	int textWidth;
	Color podiumColor;
	Color borderColor = player->color;
	borderColor.r /= 4;
	borderColor.g /= 4;
	borderColor.b /= 5;

	switch(player->deathRank){
		case 0:
			podiumColor = (Color){224, 169, 41, 255};
			break;
		case 1:
			podiumColor = (Color){175, 176, 176, 255};
			break;
		case 2:
		default:
			podiumColor = (Color){155, 104, 85, 255};
			break;
	}

	DrawRectangle(segmentAt.x - 50, segmentAt.y + segmentSize/2, 100, 40, podiumColor);
	snprintf(text, sizeof(text), "%i", player->deathRank + 1);
	textWidth = MeasureText(text, 30);
	DrawText(text, segmentAt.x - textWidth/2, segmentAt.y + segmentSize/2 + 2, 35, Fade(BLACK, 0.5));
	textWidth = MeasureText(player->name, 20);
	DrawText(player->name, segmentAt.x - textWidth/2, segmentAt.y + segmentSize/2 + 60, 20, RAYWHITE);

	for(int segNum = 0; segNum < length; segNum++){
		float phase = (isAlive ? 1 : 0.4) * sinf( 0.5 * segNum + (isAlive ? 12 : 9) * client.time + 8 * playerId );
		DrawRectangle(segmentAt.x - segmentSize / 2, segmentAt.y - segmentSize / 2, segmentSize, segmentSize, player->color);
		DrawRectangleLines(segmentAt.x - segmentSize / 2, segmentAt.y - segmentSize / 2, segmentSize, segmentSize, borderColor);
		if(segNum == length - 1){
			Vector2 leftEye = (Vector2){ segmentAt.x - 8, segmentAt.y - 5};
			Vector2 rightEye = (Vector2){ segmentAt.x + 8, segmentAt.y - 5};
			float leftEyeSize = 8 + 2 * sinf(5 * client.time + 3 * playerId );
			float rightEyeSize = 8 + 2 * sinf(5 * client.time + 3 * playerId + 130 );
			Vector2 smile_a1 = (Vector2){ leftEye.x - 4, leftEye.y + 10 };
			Vector2 smile_a2 = (Vector2){ leftEye.x + 4, leftEye.y + 14 + 3 * phase };
			Vector2 smile_b2 = (Vector2){ rightEye.x - 4, rightEye.y + 14 + 3 * phase };
			Vector2 smile_b1 = (Vector2){ rightEye.x + 4, rightEye.y + 10 };
			DrawLineEx(segmentAt, (Vector2){segmentAt.x - 14, segmentAt.y - 16}, 2, player->color);
			DrawLineEx(segmentAt, (Vector2){segmentAt.x + 14, segmentAt.y - 16}, 2, player->color);
			DrawLineBezierCubic(smile_a1, smile_b1, smile_a2, smile_b2, 2, BLACK);
			DrawCircle(leftEye.x, leftEye.y, leftEyeSize, RAYWHITE);
			DrawCircle(rightEye.x, rightEye.y, rightEyeSize, RAYWHITE);
			DrawCircleLines(leftEye.x, leftEye.y, leftEyeSize, borderColor);
			DrawCircleLines(rightEye.x, rightEye.y, rightEyeSize, borderColor);
			if(isAlive){
				DrawCircle(leftEye.x, leftEye.y, 3, BLACK);
				DrawCircle(rightEye.x, rightEye.y, 3, BLACK);
				break;
			}else{
				DrawLine(leftEye.x - 4, leftEye.y - 4, leftEye.x + 4, rightEye.y + 4, BLACK);
				DrawLine(leftEye.x + 4, leftEye.y - 4, leftEye.x - 4, rightEye.y + 4, BLACK);
				DrawLine(rightEye.x - 4, rightEye.y - 4, rightEye.x + 4, rightEye.y + 4, BLACK);
				DrawLine(rightEye.x + 4, rightEye.y - 4, rightEye.x - 4, rightEye.y + 4, BLACK);
			}
		}
		segmentAt.x += phase;
		segmentAt.y -= segmentSize - 0.2 * powf(phase, 2);
	}
	
}

static void DrawRoundEndScreen(Player_t* localPlayer){
	char text[2048];
	Color bgColor = (Color){32, 36, 63, 255};
	Color primaryColor = (Color){242, 231, 170, 255};
	Color secondaryColor = (Color){175, 178, 200, 255};
	Rectangle bounds = FsRect();

	float transition = MIN(1, roundEndTime / 2.5f);
	float cubed = 1 - powf(1 - transition, 3);

	DrawRectangle(0, 0, client.width * cubed + 10, client.height, secondaryColor);
	DrawRectangle(0, 0, client.width * cubed, client.height, bgColor);

	if(transition > 0.5){
		int maxRank = 2;
		float transition2 = (transition - 0.5) * 2;
		Color bgColor = Fade(DARKGRAY, 0.25 * transition2);
		int podiumNum = 0;

		Extend(&bounds, -50);
		DrawRectangleGradientEx(bounds, CLEAR, bgColor, bgColor, CLEAR);
		for(int rank = 0; rank < maxRank; rank++){
			forEachPlayer(player, playerId){
				if(player->deathRank > maxRank) maxRank = player->deathRank;
				if(player->deathRank == rank){
					int fontSize = 20;
					Rectangle lineBounds = CutTop(&bounds, fontSize + 2);
					Color color = player->guyCount ? primaryColor : secondaryColor;
					snprintf(text, sizeof(text), "%i. %s", rank + 1, player->name);
					DrawText(text, lineBounds.x, lineBounds.y, fontSize, color);
				}
			}
		}

		for(int rank = 0; rank < MIN(maxRank + 1, 3); rank++){
			forEachPlayer(player, playerId){
				if(rank == player->deathRank){
					Vector2 podiumSpot = (Vector2){ client.halfWidth + 170 * ((podiumNum + 1) / 2) * ((podiumNum & 1) ? -1 : 1), client.height * 0.7 + 10 * podiumNum };
					DrawPlayer(player, playerId, podiumSpot);
					podiumNum++;
				}
			}
		}
	}

	roundEndTime += client.deltaTime;
}

void DrawHUD(){
	Player_t* localPlayer = GetPlayer(client.playerId);

	if(game.state == STATE_ENDED){
		DrawRoundEndScreen(localPlayer);
	}else{
		if(localPlayer->guyCount){
			deathTime = 0;
		}else if(game.state == STATE_PLAYING){
			const float fadeInDuration = 1.0;
			const int fontSize = 30;
			Rectangle bounds;
			float fade;
			deathTime += client.deltaTime;
			fade = 1 - powf(1 - MIN(1, deathTime / fadeInDuration), 3);
			bounds = (Rectangle){ client.halfWidth - 200 * fade, client.height - 200, 400 * fade, 70 };
			DrawRectangleRec(bounds, Fade(DARKGRAY, 0.5));
			if(fade >= 1){
				const char* label = "Press SPACE to restart";
				int textWidth = MeasureText(label, fontSize);
				DrawText(label, bounds.x + (bounds.width - textWidth) / 2, bounds.y + 25, fontSize, RAYWHITE);

				if(IsKeyPressed(KEY_SPACE)){
					ResetGame();
				}
			}
		}
		roundEndTime = 0;
	}
}

#endif