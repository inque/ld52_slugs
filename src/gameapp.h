#pragma once

#define GAME_TITLE "LD54_SLUGS"
#define TICKS_PER_SEC 4
#define GET_GAME_TICKRATE() (int)(app.config.tickrate * app.speed)

#ifdef _MSC_VER
	//disable float conversion warnings
	#pragma warning(disable:4244 4305)
#endif

#include "a_player.h"
#include "a_board.h"
#include "settings.h"
#include "gameclient.h"
#include "containers.h"
#include "raylib.h"
#include "limits.h"

typedef enum{
	STATE_PREPARING,
	STATE_STARTING,
	STATE_PLAYING,
	STATE_ENDED
} GameState_e;

typedef struct{
	int frame;
	int timer;
	GameState_e state;
	Board_t board;
	COLLECTION_TYPE(PLAYERS) players;
} GameState_t;

typedef struct{
	char* configPath;
	AppConfig_t config;
	bool headless;
	bool shouldRestartGame;
	float speed;
	int gamesPlayed;
} AppState_t;

extern AppState_t app;
extern GameState_t game;

void LoadAssets();
void GameTick();
void DrawGraphics();
void StartGame();
void ResetGame();
void CloseGame();
