#pragma once
#include "gameapp.h"

static inline Player_t* GetPlayer (int id){ return GET_ENTITY(PLAYERS, id); }

#define forEachPlayer(Handle, Id) FOR_EACH(PLAYERS, Handle, Id)
